 <a href="https://vuejs.org">**Vue**</a>
 
Getting Started

• Primeiro vamos importar o arquivo JavaScript do VueJs diretamente em nossa página, porém você pode navegar pelos códigos-fonte do pacote NPM em cdn.jsdelivr.net/npm/vue.

No site do Vue.js: https://vuejs.org/ utilizaremos o script em modo de desenvolvimento.
                                      
![alt text](<./imgs/img1.PNG>) 

•Adicionaremos o script no nosso html:           
                                           
![alt text](<./imgs/img2.PNG>) 

•Agora vamos instanciar o Vue que já está importado, vale ressaltar que o Vue.js é instanciado através de componentes.
 
![alt text](<./imgs/img3.PNG>) 

•Primeiro criamos o El: Onde é o container HTML que o app vai estar inserido, basta passamos a # e o nome do id. Os dados e o DOM agora estão vinculados e tudo agora é reativo.     
                                              
![alt text](<./imgs/img4.PNG>) 

Agora criamos um html simples         
                                        
![alt text](<./imgs/img5.PNG>) 

Veja o resultado no seu navegador:       
                                          
![alt text](<./imgs/img6.PNG>) 

Agora vamos adicionar uma propriedade que é o template, e adicionar o conteúdo do nosso HTML ao componente.
 
![alt text](<./imgs/img7.PNG>) 

E ao ver como ficou no nosso navegador, percebemos que está igual ao anterior, porém se formos no código fonte, vemos que temos apenas a div app, isso porque o Vue.js pegou o template que criamos e adicionou a div com id app.
 
![alt text](<./imgs/img8.PNG>) 

Agora vamos criar uma propriedade chamada Data(), uma função que deve retornar um objeto. E criamos um array de comentários.
 
![alt text](<./imgs/img9.PNG>) 


Podemos adicionar agora um for a minha div para interar os comentários.
Nisso criamos a variável comment que vai receber em cada interação o valor que está em comments.                                                       
 
![alt text](<./imgs/img10.PNG>) 

E agora vamos acessar o conteúdo    
                                                             
![alt text](<./imgs/img11.PNG>) 

Agora vamos adicionar um evento e passar a função que queremos executar, no caso se chama addComment.
 
![alt text](<./imgs/img12.PNG>) 

Agora vamos passar uma propriedade chamada methods, e invocar a função que pega os comentários.
 
![alt text](<./imgs/img13.PNG>) 

Indo no console, já podemos perceber que o evento está sendo chamado a nossa função addComment.
 
![alt text](<./imgs/img14.PNG>) 

Agora precisamos guardar os campos nome e comentário           
                                                
![alt text](<./imgs/img15.PNG>) 

E agora vamos ligar tudo que recebemos nesse campo através do evento v-model=”” o Vue.js já vai se encarregar de popular as propriedades.
 
![alt text](<./imgs/img16.PNG>) 

Para acessar os campos nome e comentário, usamos o this.  
                                                     
![alt text](<./imgs/img17.PNG>) 

Se adicionarmos um novo nome e comentário e formos novamente no console veremos que os campos já estão sendo acessados.
 
![alt text](<./imgs/img18.PNG>) 

Adicionamos ao nosso array de comentários que ele irá popular essa lista.
 
![alt text](<./imgs/img19.PNG>) 


![alt text](<./imgs/img20.PNG>) 

Agora vamos adicionar um evento e passar a função que queremos executar, que é a função de excluir. Quando chamar a função vamos precisar capturar a posição do array na hora da interação. E vamos passar o index para a função removeComment().
 
![alt text](<./imgs/img21.PNG>) 

Esse método splice vai fazer a remoção da posição, no caso index e quantos após ele. No caso queremos excluir apenas um comentário.
 
![alt text](<./imgs/img22.PNG>) 

Agora vamos ver a exclusão do comentário!   
 
                                                      
![alt text](<./imgs/imglast.PNG>) 


Esse foi o tutorial, o código está disponibilizado no GitLab através do link: https://gitlab.com/allandasoares7/tutorialvuejs
 







